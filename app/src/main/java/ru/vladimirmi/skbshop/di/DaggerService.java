package ru.vladimirmi.skbshop.di;

import android.content.Context;

import flow.Flow;
import ru.vladimirmi.skbshop.core.App;
import ru.vladimirmi.skbshop.core.BaseScreen;
import ru.vladimirmi.skbshop.di.modules.LocaleModule;
import ru.vladimirmi.skbshop.di.modules.NetworkModule;
import ru.vladimirmi.skbshop.features.root.DaggerRootActivityComponent;
import ru.vladimirmi.skbshop.features.root.RootActivityComponent;
import ru.vladimirmi.skbshop.features.root.RootActivityModule;

/**
 * Developer Vladimir Mikhalev 21.03.2017
 */

public class DaggerService {

    private static AppComponent sAppComponent;
    private static RootActivityComponent sRootActivityComponent;

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    public static RootActivityComponent getRootActivityComponent() {
        if (sRootActivityComponent == null) {
            createRootActivityComponent();
        }
        return sRootActivityComponent;
    }

    public static void createAppComponent(Context baseContext) {
        sAppComponent = DaggerAppComponent.builder()
                .localeModule(new LocaleModule(App.getAppContext()))
                .networkModule(new NetworkModule())
                .build();
    }

    public static void createRootActivityComponent() {
        sRootActivityComponent = DaggerRootActivityComponent.builder()
                .appComponent(sAppComponent)
                .rootActivityModule(new RootActivityModule())
                .build();
    }

    @SuppressWarnings("unchecked")
    public static <T> T getComponent(Context context) {
        //noinspection ResourceType
        BaseScreen screen = Flow.getKey(context);
        if (screen != null) {
            return (T) Flow.getService(screen.getScopeName(), context);
        }
        return (T) getAppComponent();
    }

    @SuppressWarnings("unchecked")
    public static <T> T getComponent(BaseScreen screen) {
        return (T) Flow.getService(screen.getScopeName(), App.getAppContext());
    }
}
