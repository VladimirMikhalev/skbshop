package ru.vladimirmi.skbshop.di;

import javax.inject.Singleton;

import dagger.Component;
import ru.vladimirmi.skbshop.data.managers.DataManager;
import ru.vladimirmi.skbshop.di.modules.LocaleModule;
import ru.vladimirmi.skbshop.di.modules.NetworkModule;

/**
 * Developer Vladimir Mikhalev 23.03.2017
 */

@Singleton
@Component(modules = {NetworkModule.class, LocaleModule.class})
public interface AppComponent {
    DataManager dataManager();
}
