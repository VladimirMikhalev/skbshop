package ru.vladimirmi.skbshop.di.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.vladimirmi.skbshop.data.managers.PreferencesManager;

/**
 * Developer Vladimir Mikhalev 21.03.2017
 */

@Module
public class LocaleModule {
    final Context mContext;

    public LocaleModule(Context context) {
        mContext = context;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mContext;
    }

    @Provides
    @Singleton
    PreferencesManager providePreferencesManager(Context context) {
        return new PreferencesManager(context);
    }
}
