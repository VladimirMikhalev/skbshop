package ru.vladimirmi.skbshop.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.vladimirmi.skbshop.data.managers.DataManager;

/**
 * Developer Vladimir Mikhalev, 06.11.2016.
 */

@Module
public class ModelModule {

    @Provides
    @Singleton
    DataManager provideDataManager() {
        return new DataManager();
    }

}