package ru.vladimirmi.skbshop.data.network.api;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.vladimirmi.skbshop.data.network.models.GithubRepoRes;

/**
 * Developer Vladimir Mikhalev 13.03.2017
 */

public interface RestService {

    @GET("users/{user}/repos")
    Single<List<GithubRepoRes>> getRepositories(@Path("user") String user, @Query("page") int page);
}
