package ru.vladimirmi.skbshop.data.managers;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.vladimirmi.skbshop.data.network.api.RestService;

/**
 * Developer Vladimir Mikhalev 21.03.2017
 */

@Singleton
public class DataManager {

    @Inject RestService mRestService;
    @Inject PreferencesManager mPreferencesManager;

    @Inject
    public DataManager() {
    }
}
