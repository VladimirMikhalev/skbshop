package ru.vladimirmi.skbshop.utils;

/**
 * Developer Vladimir Mikhalev 13.03.2017
 */

public interface AppConfig {
    String BASE_URL = "https://api.github.com/";
    int CONNECT_TIMEOUT = 5000;
    int READ_TIMEOUT = 5000;
    int WRITE_TIMEOUT = 5000;
}
