package ru.vladimirmi.skbshop.features.login;

import io.reactivex.Single;
import ru.vladimirmi.skbshop.core.IInteractor;

/**
 * Developer Vladimir Mikhalev 23.03.2017
 */

public interface ILoginInteractor extends IInteractor {
    Single<Boolean> login(LoginViewModel viewModel);
}
