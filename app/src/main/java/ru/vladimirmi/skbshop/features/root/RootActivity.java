package ru.vladimirmi.skbshop.features.root;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.vladimirmi.skbshop.BuildConfig;
import ru.vladimirmi.skbshop.R;
import ru.vladimirmi.skbshop.di.DaggerService;
import ru.vladimirmi.skbshop.features.login.LoginScreen;
import ru.vladimirmi.skbshop.features.login.LoginViewModel;
import ru.vladimirmi.skbshop.flow.FlowActivity;
import timber.log.Timber;

/**
 * Developer Vladimir Mikhalev 20.03.2017
 */

public class RootActivity extends FlowActivity implements IRootView {

    @Inject RootPresenter mPresenter;

    @BindView(R.id.drawer) DrawerLayout mDrawer;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.root_frame) ViewGroup mRoot;
    @BindView(R.id.toolbar_logo) ImageView mToolbarLogo;

    private ActionBarDrawerToggle mToggle;
    private ActionBar mActionBar;

    //region =============== Life cycle ==============

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_root);
        ButterKnife.bind(this);
        mDispatcher.setRoot(mRoot);

        initToolbar();
        initDrawer();
        initDagger();
        mPresenter.takeView(this);
    }

    @Override
    protected void onResume() {
        if (!mPresenter.hasView()) {
            mPresenter.takeView(this);
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.dropView();
    }

    //endregion

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();
    }

    private void initDrawer() {
        mToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar,
                R.string.nav_drawer_open, R.string.nav_drawer_close);
        mToggle.syncState();
        mDrawer.addDrawerListener(mToggle);
    }

    private void initDagger() {
        DaggerService.getRootActivityComponent().inject(this);
    }


    @Override
    protected Object getDefaultKey() {
        return new LoginScreen(new LoginViewModel());
    }


    //region =============== IRootView ==============


    @Override
    public void enableDrawer(boolean enabled) {
        if (enabled) {
            mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        } else {
            mDrawer.closeDrawer(GravityCompat.START);
            mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    @Override
    public void setToolbarVisible(boolean visible) {
        if (visible) {
            mToolbar.setVisibility(View.VISIBLE);
        } else {
            mToolbar.setVisibility(View.GONE);
        }
    }

    @Override
    public void setToolbarTitle(@StringRes int titleId) {
        if (titleId == R.string.logo) {
            mToolbarLogo.setVisibility(View.VISIBLE);
        } else {
            mToolbarLogo.setVisibility(View.GONE);
            mToolbar.setTitle(titleId);
        }
    }

    @Override
    public void setHomeAsUpIndicator(boolean enabled) {
        if (mToggle != null && mActionBar != null) {
            if (enabled) {
                mToggle.setDrawerIndicatorEnabled(false);
                mActionBar.setDisplayHomeAsUpEnabled(true);
                if (mToggle.getToolbarNavigationClickListener() == null) {
                    mToggle.setToolbarNavigationClickListener(v -> onBackPressed());
                }
            } else {
                mToggle.setDrawerIndicatorEnabled(true);
                mToggle.setToolbarNavigationClickListener(null);
                mActionBar.setDisplayHomeAsUpEnabled(false);
            }
            mToggle.syncState();
        }
    }

    private ProgressDialog progressDialog;

    @Override
    public void showLoading() {
        hideLoading();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(this.getString(R.string.please_wait));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        if (progressDialog != null) {
            progressDialog.hide();
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void showMessage(@StringRes int stringId) {
        Snackbar.make(mDrawer, getString(stringId), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(String string) {
        Snackbar.make(mDrawer, string, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        showMessage(e.getLocalizedMessage());
        if (BuildConfig.DEBUG) {
            Timber.e(e, e.getLocalizedMessage());
        } else {
            // TODO: send error stacktrace to crashlytics
        }
    }

    //endregion
}
