package ru.vladimirmi.skbshop.features.login;

import ru.vladimirmi.skbshop.core.BaseViewModel;
import ru.vladimirmi.skbshop.di.DaggerScope;

/**
 * Developer Vladimir Mikhalev 23.03.2017
 */

@DaggerScope(LoginScreen.class)
public class LoginViewModel extends BaseViewModel {
    private String userName = "";
    private String password = "";
    private boolean isLoading = false;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }
}
