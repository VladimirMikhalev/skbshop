package ru.vladimirmi.skbshop.features.root;

import javax.inject.Inject;

import ru.vladimirmi.skbshop.core.BaseInteractor;
import ru.vladimirmi.skbshop.di.DaggerScope;

/**
 * Developer Vladimir Mikhalev 20.03.2017
 */

@DaggerScope(RootActivity.class)
class RootInteractor extends BaseInteractor implements IRootInteractor {

    @Inject
    public RootInteractor() {
    }
}
