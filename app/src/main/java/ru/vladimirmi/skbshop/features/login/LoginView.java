package ru.vladimirmi.skbshop.features.login;

import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import ru.vladimirmi.skbshop.R;
import ru.vladimirmi.skbshop.core.BaseView;
import ru.vladimirmi.skbshop.di.DaggerService;

/**
 * Developer Vladimir Mikhalev 21.03.2017
 */

public class LoginView extends BaseView<LoginPresenter>
        implements ILoginView {

    public LoginView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private LoginViewModel mViewModel;

    @BindView(R.id.username) EditText mUsername;
    @BindView(R.id.password) EditText mPassword;
    @BindView(R.id.submit) Button mSubmit;

    @OnTextChanged(R.id.username)
    public void onUsernameChanged(Editable username) {
        presenter.updateUsername(username.toString());
    }

    @OnTextChanged(R.id.password)
    public void onPasswordChanged(Editable password) {
        presenter.updatePassword(password.toString());
    }

    @OnClick(R.id.submit)
    public void login() {
        presenter.login();
    }

    @Override
    protected void initDagger(Context context) {
        LoginScreen.Component component = DaggerService.getComponent(context);
        component.inject(this);
    }

    //region =============== ILoginView ==============

    @Override
    public void setViewModel(LoginViewModel viewModel) {
        mViewModel = viewModel;
        mUsername.setText(viewModel.getUserName());
        mPassword.setText(viewModel.getPassword());
    }

    @Override
    public void handleLoginSuccess() {

    }

    //endregion
}
