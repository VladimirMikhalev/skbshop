package ru.vladimirmi.skbshop.features.root;

import android.support.annotation.StringRes;

import ru.vladimirmi.skbshop.core.IView;

/**
 * Developer Vladimir Mikhalev 23.03.2017
 */

public interface IRootView extends IView {
    void enableDrawer(boolean enabled);

    void setToolbarVisible(boolean visible);

    void setToolbarTitle(@StringRes int titleId);

    void setHomeAsUpIndicator(boolean enabled);

    void showLoading();

    void hideLoading();

    void showMessage(@StringRes int stringId);

    void showMessage(String string);

    void showError(Throwable e);
}
