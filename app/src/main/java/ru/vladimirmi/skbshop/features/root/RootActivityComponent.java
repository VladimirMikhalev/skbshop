package ru.vladimirmi.skbshop.features.root;

import dagger.Component;
import ru.vladimirmi.skbshop.di.AppComponent;
import ru.vladimirmi.skbshop.di.DaggerScope;

/**
 * Developer Vladimir Mikhalev 21.03.2017
 */

@DaggerScope(RootActivity.class)
@Component(dependencies = AppComponent.class,
        modules = {RootActivityModule.class})
public interface RootActivityComponent {

    IRootInteractor rootInteractor();

    RootPresenter rootPresenter();

    void inject(RootActivity view);

    void inject(RootPresenter presenter);
}
