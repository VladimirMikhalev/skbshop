package ru.vladimirmi.skbshop.features.root;

import ru.vladimirmi.skbshop.core.IInteractor;

/**
 * Developer Vladimir Mikhalev 23.03.2017
 */

interface IRootInteractor extends IInteractor {
}
