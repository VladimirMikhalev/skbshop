package ru.vladimirmi.skbshop.features.login;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Single;
import ru.vladimirmi.skbshop.core.BaseInteractor;
import ru.vladimirmi.skbshop.di.DaggerScope;

/**
 * Developer Vladimir Mikhalev 21.03.2017
 */

@DaggerScope(LoginScreen.class)
public class LoginInteractor extends BaseInteractor implements ILoginInteractor {

    @Inject
    public LoginInteractor() {
    }

    @Override
    public Single<Boolean> login(LoginViewModel viewModel) {
        // FIXME: 23.03.2017 STAB!!!
        return Single.just(true).delay(3000, TimeUnit.MILLISECONDS);
    }
}
