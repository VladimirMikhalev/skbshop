package ru.vladimirmi.skbshop.features.login;

import dagger.Provides;
import ru.vladimirmi.skbshop.R;
import ru.vladimirmi.skbshop.core.BaseScreen;
import ru.vladimirmi.skbshop.di.DaggerScope;
import ru.vladimirmi.skbshop.features.root.RootActivityComponent;

/**
 * Developer Vladimir Mikhalev 20.03.2017
 */

public class LoginScreen extends BaseScreen<RootActivityComponent, LoginViewModel> {

    public LoginScreen(LoginViewModel viewModel) {
        super(viewModel);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.screen_login;
    }

    @Override
    protected void initBuilder() {
        mBuilder = new ScreenBuilder()
                .setDrawerEnabled(false)
                .setToolbarVisible(false);
    }


    //region =============== DI ==============

    @Override
    public Object createScreenComponent(RootActivityComponent parentComponent) {
        return DaggerLoginScreen_Component.builder()
                .rootActivityComponent(parentComponent)
                .module(new Module())
                .build();
    }

    @dagger.Module
    class Module {

        @Provides
        ILoginInteractor provideLoginInteractor() {
            return new LoginInteractor();
        }

        @Provides
        @DaggerScope(LoginScreen.class)
        LoginPresenter provideLoginPresenter() {
            return new LoginPresenter(LoginScreen.this);
        }
    }

    @DaggerScope(LoginScreen.class)
    @dagger.Component(dependencies = RootActivityComponent.class,
            modules = Module.class)
    interface Component {

        void inject(LoginView loginView);

        void inject(LoginPresenter loginPresenter);
    }

    //endregion

}
