package ru.vladimirmi.skbshop.features.login;

import ru.vladimirmi.skbshop.core.IView;

/**
 * Developer Vladimir Mikhalev 23.03.2017
 */

public interface ILoginView extends IView {

    void setViewModel(LoginViewModel viewModel);

    void handleLoginSuccess();
}
