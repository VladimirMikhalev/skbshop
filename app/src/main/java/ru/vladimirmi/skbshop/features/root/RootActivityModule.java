package ru.vladimirmi.skbshop.features.root;

import dagger.Module;
import dagger.Provides;
import ru.vladimirmi.skbshop.di.DaggerScope;

/**
 * Developer Vladimir Mikhalev 23.03.2017
 */

@Module
public class RootActivityModule {

    @Provides
    @DaggerScope(RootActivity.class)
    IRootInteractor provideIRootInteractor() {
        return new RootInteractor();
    }
}
