package ru.vladimirmi.skbshop.features.login;

import io.reactivex.android.schedulers.AndroidSchedulers;
import ru.vladimirmi.skbshop.R;
import ru.vladimirmi.skbshop.core.BaseScreenPresenter;
import ru.vladimirmi.skbshop.di.DaggerService;

/**
 * Developer Vladimir Mikhalev 21.03.2017
 */

public class LoginPresenter extends BaseScreenPresenter<ILoginView, ILoginInteractor, LoginScreen> {

    private final LoginViewModel mViewModel;

    public LoginPresenter(LoginScreen screen) {
        super(screen);
        mViewModel = screen.getViewModel();
    }

    @Override
    public void dropView() {
        getRootView().hideLoading();
        super.dropView();
    }

    @Override
    protected void initDagger(ILoginView view) {
        LoginScreen.Component component = DaggerService.getComponent(((LoginView) view).getContext());
        component.inject(this);
    }

    @Override
    protected void initView(ILoginView view) {
        view.setViewModel(mViewModel);
        if (mViewModel.isLoading()) {
            login();
        }
    }


    public void updateUsername(String username) {
        mViewModel.setUserName(username);
    }

    public void updatePassword(String password) {
        mViewModel.setPassword(password);
    }

    public void login() {
        showLoading();
        mCompDisp.add(mInteractor.login(mViewModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new ViewSingleObserver<Boolean>() {
                    @Override
                    public void onSuccess(Boolean success) {
                        hideLoading();
                        if (success) {
                            mView.handleLoginSuccess();
                        } else {
                            getRootView().showMessage(R.string.app_name);
                        }
                    }
                }));
    }

    private void showLoading() {
        mViewModel.setLoading(true);
        getRootView().showLoading();
    }

    private void hideLoading() {
        mViewModel.setLoading(false);
        getRootView().hideLoading();
    }
}
