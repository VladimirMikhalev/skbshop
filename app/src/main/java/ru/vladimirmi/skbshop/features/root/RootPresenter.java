package ru.vladimirmi.skbshop.features.root;

import javax.inject.Inject;

import ru.vladimirmi.skbshop.core.BasePresenter;
import ru.vladimirmi.skbshop.di.DaggerScope;
import ru.vladimirmi.skbshop.di.DaggerService;

/**
 * Developer Vladimir Mikhalev 20.03.2017
 */

@DaggerScope(RootActivity.class)
public class RootPresenter extends BasePresenter<IRootView, IRootInteractor> {

    @Inject
    public RootPresenter() {
    }

    @Override
    protected void initDagger(IRootView view) {
        DaggerService.getRootActivityComponent().inject(this);
    }

    @Override
    protected void initView(IRootView view) {

    }
}
