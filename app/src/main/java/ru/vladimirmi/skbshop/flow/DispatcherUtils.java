package ru.vladimirmi.skbshop.flow;

import android.view.View;

import flow.Flow;
import flow.History;
import flow.State;
import flow.Traversal;

/**
 * Created by Zhuinden on 2016.07.01
 */

public class DispatcherUtils {

    public static boolean isPreviousKeySameAsNewKey(History origin, History destination) {
        return origin != null && origin.top().equals(destination.top());
    }

    public static <T> T getNewKey(Traversal traversal) {
        return traversal.destination.top();
    }

    public static <T> T getPreviousKey(Traversal traversal) {
        T previousKey = null;
        if (traversal.origin != null) {
            previousKey = traversal.origin.top();
        }
        return previousKey;
    }

    public static void persistViewToStateAndNotifyRemoval(Traversal traversal, View view) {
        persistViewToState(traversal, view);
        notifyViewForFlowRemoval(view);
    }

    public static void restoreViewFromState(Traversal traversal, View view) {
        if (view != null) {
            if (Flow.getKey(view.getContext()) != null) {
                @SuppressWarnings("ConstantConditions")
                State incomingState = traversal.getState(Flow.getKey(view.getContext()));
                incomingState.restore(view);
            }
            if (view instanceof FlowLifecycles.ViewLifecycleListener) {
                ((FlowLifecycles.ViewLifecycleListener) view).onViewRestored();
            }
        }
    }

    private static void persistViewToState(Traversal traversal, View view) {
        if (view != null) {
            if (view instanceof FlowLifecycles.PreSaveViewStateListener) {
                ((FlowLifecycles.PreSaveViewStateListener) view).preSaveViewState();
            }
            if (Flow.getKey(view.getContext()) != null) {
                @SuppressWarnings("ConstantConditions")
                State outgoingState = traversal.getState(Flow.getKey(view.getContext()));
                outgoingState.save(view);
            }
        }
    }

    private static void notifyViewForFlowRemoval(View view) {
        if (view != null && view instanceof FlowLifecycles.ViewLifecycleListener) {
            ((FlowLifecycles.ViewLifecycleListener) view).onViewDestroyed(true);
        }
    }
}
