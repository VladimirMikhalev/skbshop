package ru.vladimirmi.skbshop.flow;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import flow.Traversal;
import flow.TraversalCallback;
import ru.vladimirmi.skbshop.core.BaseScreen;

/**
 * Developer Vladimir Mikhalev 20.03.2017
 */

public class FlowDispatcher extends BaseDispatcher {

    @Override
    public void dispatch(@NonNull Traversal traversal, @NonNull TraversalCallback callback) {
        if (DispatcherUtils.isPreviousKeySameAsNewKey(traversal.origin, traversal.destination)) {
            callback.onTraversalCompleted();
            return;
        }
        final BaseScreen newKey = DispatcherUtils.getNewKey(traversal);
        final View previousView = root.getChildAt(0);
        DispatcherUtils.persistViewToStateAndNotifyRemoval(traversal, previousView);

        @LayoutRes int newScreenLayout = newKey.getLayoutResId();
        Context flowContext = traversal.createContext(newKey, baseContext);
        LayoutInflater layoutInflater = LayoutInflater.from(flowContext);
        final View newView = layoutInflater.inflate(newScreenLayout, root, false);
        DispatcherUtils.restoreViewFromState(traversal, newView);

        root.removeView(previousView);
        root.addView(newView);
        callback.onTraversalCompleted();
    }

}
