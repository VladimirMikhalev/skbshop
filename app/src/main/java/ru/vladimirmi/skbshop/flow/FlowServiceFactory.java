package ru.vladimirmi.skbshop.flow;

import android.content.Context;
import android.support.annotation.NonNull;

import flow.Flow;
import flow.Services;
import flow.ServicesFactory;
import flow.TreeKey;
import ru.vladimirmi.skbshop.core.BaseScreen;
import ru.vladimirmi.skbshop.di.DaggerService;

/**
 * Developer Vladimir Mikhalev 21.03.2017
 */

class FlowServiceFactory extends ServicesFactory {
    private final Context baseContext;

    public FlowServiceFactory(Context baseContext) {
        this.baseContext = baseContext;
    }

    @Override
    public void bindServices(@NonNull Services.Binder services) {
        BaseScreen screen = services.getKey();

        Object screenComponent;
        if (screen instanceof TreeKey) {
            BaseScreen parentScreen = (BaseScreen) ((TreeKey) screen).getParentKey();
            String parentScopeName = parentScreen.getScopeName();
            //noinspection unchecked
            screenComponent = screen.createScreenComponent(Flow.getService(parentScopeName, baseContext));
        } else {
            //noinspection unchecked
            screenComponent = screen.createScreenComponent(DaggerService.getRootActivityComponent());
        }

        String scopeName = screen.getScopeName();
        services.bind(scopeName, screenComponent);
    }

    @Override
    public void tearDownServices(@NonNull Services services) {
        super.tearDownServices(services);
    }
}
