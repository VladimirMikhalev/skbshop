package ru.vladimirmi.skbshop.flow;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

/**
 * Created by Zhuinden on 2016.07.01
 */

public interface FlowLifecycles {
    interface BackPressListener {
        boolean onBackPressed();
    }

    interface CreateDestroyListener {
        void onCreate(Bundle bundle);

        void onDestroy();
    }

    interface StartStopListener {
        void onStart();

        void onStop();
    }

    interface ResumePauseListener {
        void onResume();

        void onPause();
    }

    interface ViewLifecycleListener {
        void onViewRestored();

        void onViewDestroyed(boolean removedByFlow);
    }

    interface PreSaveViewStateListener {
        void preSaveViewState();
    }

    interface ViewStatePersistenceListener {
        void onSaveInstanceState(@NonNull Bundle outState);
    }

    interface ActivityResultListener {
        void onActivityResult(int requestCode, int resultCode, Intent data);
    }

    interface PermissionRequestListener {
        void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults);
    }
}
