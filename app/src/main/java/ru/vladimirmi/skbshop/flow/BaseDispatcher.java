package ru.vladimirmi.skbshop.flow;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.ViewGroup;

import flow.Dispatcher;
import flow.Flow;
import flow.Traversal;
import flow.TraversalCallback;

/**
 * Developer Vladimir Mikhalev 20.03.2017
 */

public abstract class BaseDispatcher implements Dispatcher,
        FlowLifecycles.BackPressListener, FlowLifecycles.ActivityResultListener,
        FlowLifecycles.PermissionRequestListener, FlowLifecycles.ViewLifecycleListener,
        FlowLifecycles.PreSaveViewStateListener {

    protected ViewGroup root;
    protected Context baseContext;

    public void setRoot(ViewGroup root) {
        this.root = root;
    }

    public void setBaseContext(Context baseContext) {
        this.baseContext = baseContext;
    }

    @Override
    public abstract void dispatch(@NonNull Traversal traversal, @NonNull TraversalCallback callback);

    private boolean hasActiveView() {
        return root != null && root.getChildCount() > 0;
    }

    @Override
    public void onViewRestored() {
        if (hasActiveView()) {
            FlowLifecycleProvider.onViewRestored(root.getChildAt(0));
        }
    }

    @Override
    public void onViewDestroyed(boolean removedByFlow) {
        if (hasActiveView()) {
            FlowLifecycleProvider.onViewDestroyed(root.getChildAt(0), removedByFlow);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (hasActiveView()) {
            FlowLifecycleProvider.onActivityResult(root.getChildAt(0), requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (hasActiveView()) {
            FlowLifecycleProvider.onRequestPermissionsResult(root.getChildAt(0), requestCode, permissions, grantResults);
        }
    }

    @Override
    public void preSaveViewState() {
        if (hasActiveView()) {
            FlowLifecycleProvider.preSaveViewState(root.getChildAt(0));
        }
    }

    @Override
    public boolean onBackPressed() {
        if (hasActiveView()) {
            if (FlowLifecycleProvider.onBackPressed(root.getChildAt(0))) {
                return true;
            }
        }
        Flow flow = Flow.get(baseContext);
        return flow.goBack();
    }
}
