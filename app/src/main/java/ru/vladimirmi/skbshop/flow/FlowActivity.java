package ru.vladimirmi.skbshop.flow;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import flow.Flow;

/**
 * Developer Vladimir Mikhalev 20.03.2017
 */

public abstract class FlowActivity extends AppCompatActivity {

    protected FlowDispatcher mDispatcher;

    @Override
    protected void attachBaseContext(Context newBase) {
        mDispatcher = new FlowDispatcher();
        mDispatcher.setBaseContext(this);
        newBase = Flow.configure(newBase, this)
                .defaultKey(getDefaultKey())
                .dispatcher(mDispatcher)
                .addServicesFactory(new FlowServiceFactory(newBase))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected abstract Object getDefaultKey();

    @Override
    protected void onPause() {
        mDispatcher.onViewDestroyed(false);
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mDispatcher.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mDispatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mDispatcher.preSaveViewState();
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (mDispatcher.onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }
}
