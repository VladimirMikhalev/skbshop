package ru.vladimirmi.skbshop.core;

/**
 * Developer Vladimir Mikhalev 14.03.2017
 */

public interface Presenter<V extends IView> {

    void takeView(V v);

    void dropView();
}
