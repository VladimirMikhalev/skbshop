package ru.vladimirmi.skbshop.core;

import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;

import flow.ClassKey;
import ru.vladimirmi.skbshop.R;
import ru.vladimirmi.skbshop.features.root.IRootView;

/**
 * Developer Vladimir Mikhalev, 27.11.2016.
 */

public abstract class BaseScreen<T, VM extends BaseViewModel> extends ClassKey {

    protected VM mViewModel;
    protected ScreenBuilder mBuilder;

    public BaseScreen(VM viewModel) {
        mViewModel = viewModel;
        initBuilder();
    }

    public VM getViewModel() {
        return mViewModel;
    }

    public void setViewModel(VM viewModel) {
        mViewModel = viewModel;
    }

    public ScreenBuilder getBuilder() {
        return mBuilder == null ? new ScreenBuilder() : mBuilder;
    }

    public String getScopeName() {
        return getClass().getName();
    }

    public abstract Object createScreenComponent(T parentComponent);

    public abstract
    @LayoutRes
    int getLayoutResId();

    protected abstract void initBuilder();

    public class ScreenBuilder {
        private boolean isGoBack = false;
        private boolean isToolbarVisible = true;
        private @StringRes int toolbarTitleId = R.string.logo;
        private boolean isDrawerEnabled = true;

        public ScreenBuilder setGoBack(boolean goBack) {
            isGoBack = goBack;
            if (isGoBack) {
                isDrawerEnabled = false;
            }
            return this;
        }

        public ScreenBuilder setToolbarVisible(boolean toolbarVisible) {
            isToolbarVisible = toolbarVisible;
            if (!isToolbarVisible) {
                isGoBack = false;
            }
            return this;
        }

        public ScreenBuilder setToolbarTitleId(@StringRes int toolbarTitleId) {
            this.toolbarTitleId = toolbarTitleId;
            return this;
        }

        public ScreenBuilder setDrawerEnabled(boolean drawerEnabled) {
            isDrawerEnabled = drawerEnabled;
            if (isDrawerEnabled) {
                isGoBack = false;
            }
            return this;
        }

        public void build(IRootView view) {
            view.enableDrawer(isDrawerEnabled);
            view.setToolbarVisible(isToolbarVisible);
            view.setToolbarTitle(toolbarTitleId);
            view.setHomeAsUpIndicator(isGoBack);
        }
    }
}
