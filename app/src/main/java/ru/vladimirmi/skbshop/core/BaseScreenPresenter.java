package ru.vladimirmi.skbshop.core;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import ru.vladimirmi.skbshop.features.root.IRootView;
import ru.vladimirmi.skbshop.features.root.RootPresenter;
import timber.log.Timber;

/**
 * Developer Vladimir Mikhalev 23.03.2017
 */

public abstract class BaseScreenPresenter<V extends IView, I extends IInteractor, S extends BaseScreen>
        extends BasePresenter<V, I> {
    protected final S mScreen;
    @Inject protected RootPresenter mRootPresenter;

    protected IRootView getRootView() {
        return mRootPresenter.getView();
    }

    public BaseScreenPresenter(S screen) {
        mScreen = screen;
    }

    @Override
    public void takeView(V v) {
        super.takeView(v);
        initScreen();
    }

    protected void initScreen() {
        mScreen.getBuilder().build(getRootView());
    }

    protected abstract class ViewSingleObserver<T> extends DisposableSingleObserver<T> {

        @Override
        public abstract void onSuccess(T t);

        @Override
        public void onError(Throwable e) {
            Timber.tag(getClass().getSimpleName());
            Timber.e(e);
            if (getRootView() != null) {
                getRootView().showError(e);
            }
        }

    }

    protected abstract class ViewObserver<T> extends DisposableObserver<T> {

        @Override
        public void onComplete() {
            Timber.tag(getClass().getSimpleName());
            Timber.d("onComplete observable");
        }

        @Override
        public void onError(Throwable e) {
            Timber.tag(getClass().getSimpleName());
            Timber.e(e);
            if (getRootView() != null) {
                getRootView().showError(e);
            }
        }

        @Override
        public abstract void onNext(T t);
    }
}
