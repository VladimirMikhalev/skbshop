package ru.vladimirmi.skbshop.core;

import javax.inject.Inject;

import ru.vladimirmi.skbshop.data.managers.DataManager;

/**
 * Developer Vladimir Mikhalev, 06.11.2016.
 */

public abstract class BaseInteractor {
    @Inject
    protected DataManager mDataManager;
}
