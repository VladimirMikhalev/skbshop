package ru.vladimirmi.skbshop.core;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

/**
 * Developer Vladimir Mikhalev, 27.10.2016
 */

public abstract class BasePresenter<V extends IView, I extends IInteractor> implements Presenter<V> {

    @Inject protected I mInteractor;

    protected CompositeDisposable mCompDisp;
    protected V mView;

    public final boolean hasView() {
        return mView != null;
    }

    public V getView() {
        return mView;
    }

    @Override
    public void takeView(V v) {
        Timber.tag(getClass().getSimpleName());
        Timber.d("takeView");
        this.mView = v;
        initDagger(mView);
        initView(mView);
        mCompDisp = new CompositeDisposable();
    }

    @Override
    public void dropView() {
        Timber.tag(getClass().getSimpleName());
        Timber.d("dropView");
        mView = null;
        if (!mCompDisp.isDisposed()) {
            mCompDisp.clear();
        }
    }

    protected abstract void initDagger(V view);

    protected abstract void initView(V view);
}
