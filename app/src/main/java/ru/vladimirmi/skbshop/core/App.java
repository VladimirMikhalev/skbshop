package ru.vladimirmi.skbshop.core;

import android.app.Application;
import android.content.Context;

import ru.vladimirmi.skbshop.BuildConfig;
import ru.vladimirmi.skbshop.di.DaggerService;
import timber.log.Timber;

/**
 * Developer Vladimir Mikhalev 21.03.2017
 */

public class App extends Application {

    private static Context sAppContext;

    public static Context getAppContext() {
        return sAppContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sAppContext = getBaseContext();
        DaggerService.createAppComponent(getAppContext());

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
