package ru.vladimirmi.skbshop.core;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import javax.inject.Inject;

import butterknife.ButterKnife;
import ru.vladimirmi.skbshop.flow.FlowLifecycles;
import timber.log.Timber;

/**
 * Developer Vladimir Mikhalev, 27.10.16
 */

public abstract class BaseView<P extends BasePresenter> extends FrameLayout
        implements IView, FlowLifecycles.ViewLifecycleListener {
    @Inject protected P presenter;

    public BaseView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            initDagger(context);
        }
    }

    protected abstract void initDagger(Context context);

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            ButterKnife.bind(this);
        }
        Timber.tag(getClass().getSimpleName());
        Timber.d("onFinishInflate");
    }

    @Override
    public void onViewRestored() {
        //noinspection unchecked
        presenter.takeView(this);
        Timber.tag(getClass().getSimpleName());
        Timber.d("onViewRestored");
    }

    @Override
    public void onViewDestroyed(boolean removedByFlow) {
        presenter.dropView();
        Timber.tag(getClass().getSimpleName());
        Timber.d("onViewDestroyed");
    }
}
